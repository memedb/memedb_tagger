mod app;
mod file;
mod preview;

use app::{App, Signal};
use std::io::{stdin, stdout, Write};
use termion::{
    clear::All,
    cursor::{Goto, Show},
    input::TermRead,
    raw::IntoRawMode,
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let result = run();
    print!("{}{}{}", All, Goto(1, 1), Show);
    stdout().flush()?;
    result
}

fn run() -> Result<(), Box<dyn std::error::Error>> {
    let mut app = App::new()?;

    let mut stdout = stdout().into_raw_mode()?;
    let stdin = stdin();

    app.draw(&mut stdout)?;
    for key in stdin.keys() {
        use Signal::*;
        match app.handle_key(key?)? {
            Exit => break,
            Redraw => app.draw(&mut stdout)?,
            Nothing => (),
        }
    }
    Ok(())
}
