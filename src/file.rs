use std::io::{Error as IoError, Write};
use std::{collections::HashSet, path::PathBuf};
use termion::{
    color::{Blue, Fg, Reset as ColorReset},
    style::Reset,
};

pub struct File {
    pub name: String,
    pub path: PathBuf,
    pub tags: HashSet<String>,
}

impl File {
    pub fn draw(&self, stdout: &mut impl Write) -> Result<usize, IoError> {
        let mut tags = String::new();
        for tag in &self.tags {
            tags.push_str(tag);
            tags.push(',');
        }
        tags.pop();
        write!(
            stdout,
            "{}{}:{} {}{}",
            Fg(Blue),
            self.name,
            Reset,
            Fg(ColorReset),
            tags
        )?;
        Ok(self.name.len() + 2 + tags.len())
    }
}
