# MemeDB Tagger

A simple CLI tool to tag your images with vim-ish keybinds, using the power of `memedb`.

If you are using the `kitty` terminal emulator you get image previews for free!
